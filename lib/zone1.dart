import 'dart:async';

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:pretty_gauge/pretty_gauge.dart';

class Zone1 extends StatefulWidget {
  const Zone1({Key? key}) : super(key: key);

  @override
  _Zone1State createState() => _Zone1State();
}

class _Zone1State extends State<Zone1> {


  double h1 = 0.0;
  double h2 = 0.0;
  double h3 = 0.0;
  double h4 = 0.0;
  double av = 0.0;

  String pump = "";



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Humidity Zone1"),),
      body: SingleChildScrollView(
        child: Column(
          children: [

            SizedBox(height: 30,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Column(
                  children: [
                    PrettyGauge(
                      gaugeSize: 100,
                      segments: [
                        GaugeSegment('Low', 20, Colors.blue[200]!),
                        GaugeSegment('Medium', 40, Colors.blue),
                        GaugeSegment('High', 40, Colors.blue[800]!),
                      ],

                      valueWidget: Text('${h1}%', style: TextStyle(fontSize: 12 , fontWeight: FontWeight.w400)),
                      currentValue: h1,
                      displayWidget:
                      const Text('Melon 1', style: TextStyle(fontSize: 12 , fontWeight: FontWeight.w600)),
                    ),
                  ],
                ),
                Column(
                  children: [
                    PrettyGauge(
                      gaugeSize: 100,
                      segments: [
                        GaugeSegment('Low', 20, Colors.blue[200]!),
                        GaugeSegment('Medium', 40, Colors.blue),
                        GaugeSegment('High', 40, Colors.blue[800]!),
                      ],

                      valueWidget: Text('${h2}%', style: TextStyle(fontSize: 12 , fontWeight: FontWeight.w400)),
                      currentValue: h2,
                      displayWidget:
                      const Text('Melon 2', style: TextStyle(fontSize: 12 , fontWeight: FontWeight.w600)),
                    ),
                  ],
                )
              ],
            ),
            SizedBox(height: 30,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Column(
                  children: [
                    PrettyGauge(
                      gaugeSize: 100,
                      segments: [
                        GaugeSegment('Low', 20, Colors.blue[200]!),
                        GaugeSegment('Medium', 40, Colors.blue),
                        GaugeSegment('High', 40, Colors.blue[800]!),
                      ],

                      valueWidget: Text('${h3}%', style: TextStyle(fontSize: 12 , fontWeight: FontWeight.w400)),
                      currentValue: h3,
                      displayWidget:
                      const Text('Melon 3', style: TextStyle(fontSize: 12 , fontWeight: FontWeight.w600)),
                    ),
                  ],
                ),
                Column(
                  children: [
                    PrettyGauge(
                      gaugeSize: 100,
                      segments: [
                        GaugeSegment('Low', 20, Colors.blue[200]!),
                        GaugeSegment('Medium', 40, Colors.blue),
                        GaugeSegment('High', 40, Colors.blue[800]!),
                      ],

                      valueWidget: Text('${h4}%', style: TextStyle(fontSize: 12 , fontWeight: FontWeight.w400)),
                      currentValue: h4,
                      displayWidget:
                      const Text('Melon 4', style: TextStyle(fontSize: 12 , fontWeight: FontWeight.w600)),
                    ),
                  ],
                )
              ],
            ),
            Column(
              children: [
                PrettyGauge(
                  gaugeSize: 200,
                  segments: [
                    GaugeSegment('Low', 20, Colors.blue[200]!),
                    GaugeSegment('Medium', 40, Colors.blue),
                    GaugeSegment('High', 40, Colors.blue[800]!),
                  ],

                  valueWidget: Text('${av}%', style: TextStyle(fontSize: 12 , fontWeight: FontWeight.w400)),
                  currentValue: av,
                  displayWidget:
                   Text('Average Humidity', style: TextStyle(fontSize: 12 , fontWeight: FontWeight.w600)),
                ),
              ],
            ),
            Text('${pump}', style: TextStyle(fontSize: 20 , fontWeight: FontWeight.w600)),

            Text('Solenoid Valve Status 1', style: TextStyle(fontSize: 20 , fontWeight: FontWeight.w600)),
          ],
        ),
      ),
    );
  }

  final databaseReference = FirebaseDatabase.instance.reference();

  @override
  void initState() {

    Timer.periodic(Duration(seconds: 1), (timer) {
      setState(() {
        callData();
      });
    });


  }

  callData(){
    databaseReference.child("/Zone1/Humidity1").once().then((value) => {
      setState(() {
        h1 = double.parse(value.snapshot.value.toString());
      })
    });

    databaseReference.child("/Zone1/Humidity2").once().then((value) => {
      setState(() {
        h2 = double.parse(value.snapshot.value.toString());
      })
    });


    databaseReference.child("/Zone1/Humidity3").once().then((value) => {
      setState(() {
        h3 = double.parse(value.snapshot.value.toString());
      })
    });


    databaseReference.child("/Zone1/Humidity4").once().then((value) => {
      setState(() {
        h4 = double.parse(value.snapshot.value.toString());
      })
    });

    databaseReference.child("/Zone1/SolenoidValve1").once().then((value) => {
      setState(() {
        pump = (value.snapshot.value.toString());
      })
    });

    databaseReference.child("/Zone1/AverageZone1").once().then((value) => {
      setState(() {
        av = double.parse(value.snapshot.value.toString());
      })
    });



  }




}
