import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:melon/home.dart';
import 'package:melon/setting.dart';
import 'package:melon/temp.dart';
import 'package:melon/zone1.dart';
import 'package:melon/zone2.dart';
import 'package:melon/zone3.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(

        primarySwatch: Colors.blue,
      ),
      home:  MyStatelessWidget(),
    );
  }


}



class MyStatelessWidget extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    final PageController controller = PageController();
    return PageView(
      /// [PageView.scrollDirection] defaults to [Axis.horizontal].
      /// Use [Axis.vertical] to scroll vertically.
      controller: controller,
      children: const <Widget>[
        Center(
          child: Zone1(),
        ),
        Center(
          child: Zone2(),
        ),
        Center(
          child: Zone3(),
        ),
        Center(
          child: Temp(),
        ),
        Center(
          child: SettingPage(),
        ),
      ],
    );
  }
}


