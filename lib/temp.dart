import 'dart:async';

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:pretty_gauge/pretty_gauge.dart';

class Temp extends StatefulWidget {
  const Temp({Key? key}) : super(key: key);

  @override
  _TempState createState() => _TempState();
}

class _TempState extends State<Temp> {

  final databaseReference = FirebaseDatabase.instance.reference();



  double temp = 0.0;
  double hum = 0.0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Temperature"),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: 30,
            ),
            Text(
              "Temperature and Humidity in Greenhouse",
              style: TextStyle(fontSize: 15, fontWeight: FontWeight.w600),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                PrettyGauge(
                  gaugeSize: 200,
                  segments: [
                    GaugeSegment('Low', 20, Colors.blue[200]!),
                    GaugeSegment('Medium', 40, Colors.yellow),
                    GaugeSegment('High', 40, Colors.red),
                  ],
                  valueWidget: Text('${temp}°C',
                      style:
                          TextStyle(fontSize: 12, fontWeight: FontWeight.w400)),
                  currentValue: temp,
                  displayWidget: Text('Temperature',
                      style:
                          TextStyle(fontSize: 12, fontWeight: FontWeight.w600)),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                PrettyGauge(
                  gaugeSize: 200,
                  segments: [
                    GaugeSegment('Low', 20, Colors.blue[200]!),
                    GaugeSegment('Medium', 40, Colors.blue),
                    GaugeSegment('High', 40, Colors.blue[800]!),
                  ],
                  valueWidget: Text('${hum}%',
                      style:
                          TextStyle(fontSize: 12, fontWeight: FontWeight.w400)),
                  currentValue: hum,
                  displayWidget: Text('Humidity',
                      style:
                          TextStyle(fontSize: 12, fontWeight: FontWeight.w600)),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  @override
  void initState() {

    Timer.periodic(Duration(seconds: 1), (timer) {
      setState(() {
        callData();
      });
    });
  }

  callData() {


    databaseReference.child("/Air/Humididty").once().then((value) => {
      setState(() {
        hum = double.parse(value.snapshot.value.toString());
      })
    });

    databaseReference.child("/Air/Temperature").once().then((value) => {
      setState(() {
        temp = double.parse(value.snapshot.value.toString());
      })
    });




  }

}
