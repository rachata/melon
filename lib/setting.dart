import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_core/theme.dart';
import 'package:syncfusion_flutter_sliders/sliders.dart';

class SettingPage extends StatefulWidget {
  const SettingPage({Key? key}) : super(key: key);

  @override
  _SettingPageState createState() => _SettingPageState();
}

class _SettingPageState extends State<SettingPage> {
  final databaseReference = FirebaseDatabase.instance.reference();

  double _activeSliderValue = 0;
  String pump = "";

  @override
  void initState() {
    super.initState();

    setState(() {
      callData();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Controller"),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(10),
          child: Column(
            children: [
              SizedBox(
                height: 10,
              ),
              Text("Set Average Humidity ${_activeSliderValue.toStringAsFixed(2)} %"),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    width: (MediaQuery.of(context).size.width - 20) * 0.1,
                    child: Text("0%"),
                  ),
                  Container(
                    width: (MediaQuery.of(context).size.width - 20) * 0.7,
                    child: SfSliderTheme(
                        data: SfSliderThemeData(
                            tooltipBackgroundColor: Colors.red),
                        child: SfSlider(
                          interval: 1.0,
                          min: 0.0,
                          max: 100.0,
                          onChanged: (dynamic values) {
                            setState(() {
                              _activeSliderValue = values as double;
                              databaseReference.child("/Controller/Setting").set(_activeSliderValue);

                            });
                          },
                          value: _activeSliderValue,
                          enableTooltip: true,
                        )),
                  ),
                  Container(
                    width: (MediaQuery.of(context).size.width - 20) * 0.1,
                    child: Text("100%"),
                  ),
                ],
              ),
              Text('${pump}',
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600)),
              Text('Pump Status',
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600)),
            ],
          ),
        ),
      ),
    );
  }

  callData() {
    print("callData");

    databaseReference.child("/Controller/Pump").once().then((value) => {
          setState(() {
            pump = value.snapshot.value.toString();
          })
        });

    databaseReference.child("/Controller/Setting").once().then((value) => {
      setState(() {
        _activeSliderValue = double.parse(value.snapshot.value.toString());
      })
    });




  }
}
